import Vue from 'vue'
import Router from 'vue-router'

import Layout from '@/views/GeneratorLayout/Layout'
import DesignLayout from '@/views/DesignLayout/Layout'
Vue.use(Router)
export const constantRouterMap = [
  // 构建器路由
  {
    path: '/:appId/design',
    redirect: '/:appId/design/index',
    name: 'design',
    component: DesignLayout,
    meta: { platform: 'editor' },
    children: [
      {
        path: 'index',
        name: 'design-index',
        component: () => import('@/views/builder/Home.vue')
      },
      {
        path: 'function',
        name: 'design-function',
        component: () => import('@/views/function/functionlist.vue')
      },
      {
        path: 'workflow/workflowlist',
        name: 'design-workflowlist',
        component: () => import('@/views/workflow/workflowlist.vue')
      },
      {
        path: 'workflow/createworkflow',
        name: 'design-createworkflow',
        component: () => import('@/views/workflow/createworkflow.vue')
      },
      {
        path: 'workflow/workflowboard',
        name: 'design-workflowboard',
        component: () => import('@/views/workflow/workflowboard.vue')
      },
      {
        path: 'setting',
        name: 'design-setting',
        component: () => import('@/views/setting/index.vue')
      },
      {
        path: 'roleset',
        name: 'design-roleset',
        component: () => import('@/views/setting/roleset.vue')
      },
      {
        path: 'addrole',
        name: 'design-addrole',
        component: () => import('@/views/setting/addrole.vue')
      }
    ]
  },
  {
    path: '/',
    redirect: '/:appId/design/index'
  },
  // 生成器路由
  {
    path: '/:appId/app',
    component: Layout,
    redirect: '/:appId/app/dashboard',
    name: 'app-Dashboard',
    hidden: true,
    meta: { platform: 'application', menu: false, infoShow: true },
    children: [
      {
        path: 'dashboard',
        name: 'app-dashboard',
        component: () => import('@/views/GeneratorDashboard/index.vue'),
        meta: { infoShow: true }
      },
      {
        path: 'form/:id',
        name: 'app-form',
        component: () => import('@/views/GeneratorTemplate/add.vue'),
        meta: { infoShow: true }
      },
      {
        path: 'report/:id',
        name: 'app-report',
        component: () => import('@/views/GeneratorTemplate/index.vue'),
        meta: { infoShow: true }
      },
      {
        path: 'edit/:id/:dataid',
        name: 'app-edit',
        component: () => import('@/views/GeneratorTemplate/edit.vue'),
        meta: { infoShow: true }
      },
      {
        path: 'copy/:id/:dataid',
        name: 'app-copy',
        component: () => import('@/views/GeneratorTemplate/edit.vue'),
        meta: { infoShow: true }
      }
    ]
  }
]
export default new Router({
  routes: constantRouterMap
})

