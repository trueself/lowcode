import router, { createRouter } from './router'
import { getPlatform, getAppId } from '@/util/auth'
import store from './store'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken, setPlatform } from '@/util/auth' // getToken from cookie

NProgress.configure({ showSpinner: false })// NProgress configuration

const whiteList = [] // 不重定向白名单
router.beforeEach(async(to, from, next) => {
  NProgress.start()
  if (whiteList.indexOf(to.path) !== -1) {
    next()
  } else {
    const designToFlag = to.path.indexOf('/design/index')
    const designFromFlag = from.path.indexOf('/design/index')
    const appToFlag = to.path.indexOf('/app/dashboard')
    const appFromFlag = from.path.indexOf('/app/dashboard')
    if (((designToFlag !== -1 || to.path === '/') && (designFromFlag == -1 && from.path !== '/')) ||
      ((to.path.slice(-3) === 'app' || appToFlag !== -1) && (from.path !== '/' && from.path.slice(-3) !== 'app' && appFromFlag === -1))) {
      localStorage.setItem('refresh', '1')
    } else {
      localStorage.setItem('refresh', '0')
    }
    if (getToken()) {
      try {
        const info = await store.dispatch('GetInfo')
        if (to.matched.length == 0) {
          next({ path: '/' })
        } else {
          next()
        }
      } catch (e) {
        console.log(e)
        jumpToLogin(to)
      }
    } else {
      jumpToLogin(to)
    }
  }
})
function jumpToLogin(to) {
  if (process.env.NODE_ENV === 'production') {
    // 生产环境
    // platform和appid是编辑器和构建器必须的参数，若没有则登录完不用redirect
    if (getPlatform() && getAppId()) {
      window.location.href = 'http://company.sangon.net/#/login/index?redirect=' + encodeURIComponent('https://company.sangon.net/#' + to.path)
    } else {
      window.location.href = 'http://company.sangon.net/#/login/index'
    }
  } else if (process.env.NODE_ENV === 'development') {
    // 测试环境
    if (getPlatform() && getAppId()) {
      window.location.href = 'http://localhost:9595/#/login/index?redirect=' + encodeURIComponent('http://localhost:8080/#' + to.path)
    } else {
      window.location.href = 'http://localhost:9595/#/login/index'
    }
  }
}
router.afterEach(() => {
  NProgress.done() // 结束Progress
})
